export const getUserDetails = (db, userName) => {
  return new Promise((resolve, reject) => {
    console.log(userName);
    db.collection("user")
      .find({ email: userName })
      .toArray((err, docs) => {
        if (docs && docs.length > 0) {
          resolve(docs[0]);
        } else {
          reject(new Error("COULD NOT FIND USER"));
        }
      });
  });
};
export const updateUserPassword = (db, userName, pwd) => {
  return db
    .collection("user")
    .updateOne({ username: userName }, { $set: { password: pwd } })
    .then((r) => {
      return Promise.resolve(r.matchedCount);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
};

export const signUpUser = (db, user) => {
  const returnedObj = db.collection("user").insertOne(user);
  console.log("OUTPUT OF INSERT:" + returnedObj);
};
