import express from "express";
import bodyParser from "body-parser";
import user from "./routes/user";
import { MongoClient } from "mongodb";
import { clientApiKeyValidation, ValidateAuth } from "./common/authUtils";
import { finaliseResponse } from "./common/responseUtils";

var cors = require("cors");
require("dotenv").config();

// Load .env variables here
const DB_CONN_URL = process.env.DB_URL + ":" + process.env.DB_PORT; //"mongodb://localhost:27017";
const DB_NAME = process.env.DB_NAME;
const SERVER_PORT = process.env.SERVER_PORT;

let mongoClient = null;

MongoClient.connect(DB_CONN_URL, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
  mongoClient = client;
  if (err) {
    console.log(err);
  }
});

let app = express();
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.use((req, res, next) => {
  req.db = mongoClient.db(DB_NAME);
  next();
});
app.get("/v1", (req, res, next) => {
  res.status(200).send({
    status: true,
    response: "Hello World!",
  });
});

// BS, Code moved to AuthUtils for more readability
app.use(ValidateAuth);
app.use(clientApiKeyValidation);
app.use("/v1/user", user);

// BS Code moved to responseUtils for more readability
app.use(finaliseResponse);

app.listen(SERVER_PORT, () => {
  console.log(" ********** : running on " + SERVER_PORT);
});

process.on("exit", (code) => {
  mongoClient.close();
  console.log(`About to exit with code: ${code}`);
});

process.on("SIGINT", function () {
  console.log("Caught interrupt signal");
  process.exit();
});

module.exports = app;
