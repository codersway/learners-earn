import { Router } from "express";
import { getUserDetails, updateUserPassword, signUpUser } from "../services/UserService";

let router = Router();

router.post("/session", async (req, res, next) => {
  res.data = req.session.userData;
  next();
});

router.post("/signup", async (req, res, next) => {
  const user = req.body.user;
  const { email } = user;
  await getUserDetails(req.db, email)
    .then((userDetails) => res.send("User is already registered"))
    .catch((err) => {
      let sigUp = signUpUser(req.db, user);
      if (sigUp) {
        res.send("signed you Up");
      } else {
        res.send("Not successfull");
      }
    });
});

router.post("/login", async (req, res, next) => {
  let uname = req.body.username;
  let pwd = req.body.password;

  getUserDetails(req.db, uname)
    .then((userDetails) => {
      if (userDetails) {
        let { password } = userDetails;
        if (pwd === password) {
          res.data = userDetails;
          req.session.userData = userDetails;
          console.log("Password verified");
          next();
        } else {
          res.statusCode = 403;
          res.data = {
            status: false,
            error: "Invalid Password",
          };
          res.send();
          console.log("Invalid Password");
        }
      }
    })
    .catch((err) => {
      res.statusCode = 403;
      res.data = { status: false, error: err };
      res.send();
    });
});
router.put("/password", async (req, res, nex) => {
  try {
    var oldPwd = req.body.old_password;
    var newPwd = req.body.new_password;
    if (!oldPwd && !newPwd) {
      res.statusCode(400);
      res.data = {
        status: false,
        error: "Invalid Parameters",
      };
    }
    var usname = req.session.userData.email;
    let userDetails = await getUserDetails(req.db, usname);
    if (oldPwd != userDetails.user.password) {
      res.statusCode(400);
      res.data = {
        status: false,
        error: "Old Password does not match",
      };
    } else {
      let updateRes = updateUserPassword(req.db, usname, newPwd);
      res.data = {
        message: "successfully updated the new password",
      };
    }
    next();
  } catch (e) {
    next(e);
  }
});
export default router;
