import { generateJWTToken } from "./authUtils";

export const finaliseResponse = async (req, res, next) => {
  if (!res.data) {
    return res.status(404).send({
      status: false,
      error: {
        reason: "Invalid Endpoint",
        code: 404,
      },
    });
  }
  if (req.newSessionRequired && req.session.userData) {
    try {
      res.setHeader("Session-Token", generateJWTToken(req.session.userData));
      res.data["session-token"] = generateJWTToken(req.session.userData);
    } catch (e) {
      console.log("e:", e);
    }
  }
  if (req.session && req.session.sessionID) {
    try {
      res.setHeader("Session-Token", req.session.sessionID);
      res.data["session-token"] = req.session.sessionID;
    } catch (e) {
      console.log("Error : ", e);
    }
  }
  // Remove any User secrets here before returning the response
  if (res.data && res.data.password) delete res.data.password;
  if (res.data && res.data.configmPassword) delete res.data.configmPassword;
  res.status(res.statusCode || 200).send({ status: true, response: res.data });
};
