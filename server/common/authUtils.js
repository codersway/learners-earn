import jwt from "jsonwebtoken";
import { getClientDetails } from "../services/ClientService";

const newSessionRoutes = [
  { path: "/user/login", method: "POST" },
  { path: "/user/signup", method: "POST" },
];
const authRoutes = [{ path: "/user/password", method: "PUT" }];
const SECRET_KEY = "JWT_SECRET";
// const API_KEY = "Happy123";     /// .env file se lenge
export const clientApiKeyValidation = async (req, res, next) => {
  let clientApiKey = req.get("api_key");
  console.log("API_KEY = " + clientApiKey); // Remove Later
  if (!clientApiKey) {
    return res.status(400).send({
      status: false,
      response: "Missing Api Key",
    });
  }

  try {
    /**
     * because the can possibly be only two clients currently for our app hence decided to keep the API keys into .env file
     * let clientDetails = await getClientDetails(req.db, clientApiKey);
     */
    let webClientApiKey = process.env.WEB_APP_CLIENT_API_KEY;
    let mobileClientWpiKey = process.env.MOBILE_APP_CLIENT_API_KEY;

    if (webClientApiKey === clientApiKey || mobileClientWpiKey === clientApiKey) next();
    else throw "Invalid Api Key";
  } catch (e) {
    console.log("%%%%%%%% error :", e);
    return res.status(400).send({
      status: false,
      response: "Invalid Api Key",
    });
  }
};

export const isNewSessionRequired = (httpMethod, url) => {
  for (let routeObj of newSessionRoutes) {
    if (routeObj.method === httpMethod && url.includes(routeObj.path)) {
      return true;
    }
  }
  return false;
};

export const isAuthRequired = (httpMethod, url) => {
  for (let routeObj of authRoutes) {
    if (routeObj.method === httpMethod && routeObj.path === url) {
      return true;
    }
  }
  return false;
};

export const generateJWTToken = (userData) => {
  return jwt.sign(userData, SECRET_KEY);
};
export const verifyToken = (jwtToken) => {
  try {
    return jwt.verify(jwtToken, SECRET_KEY);
  } catch (e) {
    console.log("Error:" + e);
    return null;
  }
};

// Function moved from app.js
export const ValidateAuth = async (req, res, next) => {
  var apiUrl = req.originalUrl;
  var httpMethod = req.method;
  req.session = {};
  if (isNewSessionRequired(httpMethod, apiUrl)) {
    req.newSessionRequired = true;
    // } else if (isAuthRequired(apiUrl, httpMethod)) {
  } else {
    let sessionID = req.header("Authorization");

    if (sessionID) {
      let userData = verifyToken(sessionID);
      if (userData) {
        req.session.userData = userData;
        req.session.sessionID = sessionID;
      } else {
        return res.status(401).send({
          ok: false,
          error: {
            reason: "Invalid session Token",
            code: 401,
          },
        });
      }
    } else {
      return res.status(401).send({
        ok: false,
        error: {
          reason: "Missing Session token",
          code: 401,
        },
      });
    }
  }
  next();
};
