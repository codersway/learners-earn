import React from "react";
import { useState, useContext, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import { User } from "../utils/API";
import { useHistory } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import { CircularProgress } from "@material-ui/core";

const styles = makeStyles((theme) => ({
  main: {
    marginTop: theme.spacing(15),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  linkItem: {
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  progress: {
    margin: theme.spacing(10),
  },
}));

function Login(props) {
  const classes = styles();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loginProgress, setLoginProgress] = useState(false);

  // using the UserContext
  const { setAuthToken, setUser, authToken } = useContext(UserContext);

  const history = useHistory();

  const doLogin = (e) => {
    e.preventDefault();
    setLoginProgress(true);
    User.Login({ username: username, password: password })
      .then((success) => {
        // Setting  Auth token and retrieved user
        setLoginProgress(false);
        setAuthToken(success.data.response["session-token"]);
        setUser({ ...success.data.response, loggedIn: true });
        history.push("/");
      })
      .catch((err) => {
        console.log("err : ", err);
        setLoginProgress(false);
      });
  };

  function handleUsernameChange(e) {
    setUsername(e.target.value);
  }

  function handlepasswordChange(e) {
    setPassword(e.target.value);
  }

  useEffect(() => {
    if (authToken) {
      setLoginProgress(true);
      // if there is already auth token then load the user directly
      User.VerifyAuth()
        .then((success) => {
          setUser({ ...success.data.response, loggedIn: true });
          setLoginProgress(false);
          history.push("/");
        })
        .catch((err) => {
          console.log("err : ", err);
          setLoginProgress(false);
          // Todo Error handling
        });
    } else {
      setLoginProgress(false);
    }
  }, [history, setUser, authToken]);

  return (
    <Container component="main" maxWidth="xs" className={classes.main}>
      <Typography component="h1" variant="h5">
        Sign in
      </Typography>
      {loginProgress ? (
        <CircularProgress className={classes.progress} />
      ) : (
        <form className={classes.form} onSubmit={doLogin}>
          <TextField variant="outlined" type="email" required fullWidth margin="normal" id="username" label="Email Address" onChange={handleUsernameChange} value={username} autoComplete="current-password"></TextField>
          <TextField variant="outlined" type="password" required fullWidth margin="normal" id="Password" label="Password" onChange={handlepasswordChange} value={password} autoComplete="email"></TextField>
          <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
            Login
          </Button>

          <Grid container justify="space-between">
            <Grid item xs className={classes.linkItem}>
              <Link color="primary" href="#" variant="body1">
                Forgot password?
              </Link>
            </Grid>
            <Grid item className={classes.linkItem}>
              <Link color="primary" href="/signup" variant="body1">
                Sign up
              </Link>
            </Grid>
          </Grid>
        </form>
      )}
    </Container>
  );
}

export default Login;
