import React from "react";
import SideDrawer from "../components/SideDrawer";
import Header from "../components/Header";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Content from "../components/Content";
const useStyle = makeStyles((theme) => ({
  root: {
    display: "flex",
  }
}));

function Home() {
  let [drawerOpen, setDrawerOpen] = React.useState(false);
  let [institueClicked, setInstitueClicked] = React.useState(false);
  

  const classes = useStyle();
  const drawerWidth = 270;

  const handleDrawerToggle = () => {
    drawerOpen = !drawerOpen;
    setDrawerOpen(drawerOpen);
  };

  const handleInstitueClick = () => {
    setInstitueClicked(true);
  };
  
  return (
    <div className={classes.root}>
      <SideDrawer config={{ open: drawerOpen, handleDrawerToggle}} instConfig={{institute:institueClicked,handleInstitueClick}}></SideDrawer>
      <Header sideDrawerConfig={{ open: drawerOpen, handleDrawerToggle }}></Header>
      <Content click={institueClicked}  />
    </div>
  );
}
export default Home;
