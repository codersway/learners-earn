import React, { createContext } from "react";

export const UserContext = createContext();

export class UserProvider extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      user: {
        firstName: "",
        lastName: "",
        email: "",
        loggedIn : false
      },
      authToken: localStorage.getItem("token"),
    };
    this.setAuthToken = this.setAuthToken.bind(this);
    this.setUser = this.setUser.bind(this);
  }

  setAuthToken(token) {
    localStorage.setItem("token", JSON.stringify(token));
    let {authToken} = this.state;
    authToken = token;
    this.setState({ authToken });
  }

  setUser(user) {
    this.setState({ user });
  }

  render() {
    const { children } = this.props;
    const { user, authToken } = this.state;
    const { setUser, setAuthToken } = this;

    return <UserContext.Provider value={{ authToken,setAuthToken, user, setUser }}>{children}</UserContext.Provider>;
  }
}
