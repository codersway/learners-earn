import React from "react";
import { makeStyle, makeStyles } from "@material-ui/core/styles";
import { TextField, Typography, Button } from "@material-ui/core";
const useStyle = makeStyles((theme) => ({
  root: {
    width: "50%",
    margin: "auto",
    "& .MuiTextField-root": {
      margin:theme.spacing(1),
      width: "100%",
    }
  },
  typography:{
    margin:theme.spacing(1),
    display:"flex",
    justifyContent:"center"
  },
  button:{
    margin: theme.spacing(3, 1, 2),
  }
}));

function InstituteCreate() {
  const classes = useStyle();
  return (
    <div>
      <Typography className={classes.typography} variant="h5">
        Enter Institute Details
      </Typography>
      <form className={classes.root}>
        <TextField required label="Institute Name" variant="outlined" />
        <TextField required label="Address line 1" variant="outlined" />
        <TextField required label="Address line 2" variant="outlined" />
        <TextField required label="Email" type="Email" variant="outlined" />
        <TextField required label="Contact Number" variant="outlined" />
        <Button className={classes.button} type="submit" variant="contained" color="secondary" fullWidth>
          Add
        </Button>
      </form>
    </div>
  );
}

export default InstituteCreate;
