import React from "react";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import BusinessIcon from "@material-ui/icons/Business";
import { makeStyle, makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import { ListItem, Button, Typography, Divider, IconButton, Badge } from "@material-ui/core";
import { School, LibraryBooks } from "@material-ui/icons";
const useStyle = makeStyles((theme) => ({
  root: {
      marginTop:"20px"
  },
  listItem: {
    backgroundColor: "Grey",
    display: "flex",
    width: "40%",
  },
  listContentPart1: {
    display: "flex",
    borderBottomStyle: "solid",
    paddingBottom: "5px",
    marginBottom: "10px",
  },
  listContentPart2: {
    display: "flex",
    justifyContent: "space-between",
  },
  listItemInner: {
    width: "100%",
  },
  rightContainer: {
    borderRightStyle: "solid",
    marginRight: "0",
    width:"50%"
  },
  leftContainer:{
   width:"50%",
   textAlign: "right"
  }
}));

function InstituteList(props) {
  const classes = useStyle();
  const{handleAddClick}=props.config;
  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleAddClick}>
        Add Institute
        <AddCircleIcon />
      </Button>
      <List className={classes.root}>
        <ListItem className={classes.listItem} button>
          <div className={classes.listItemInner}>
            <div className={classes.listContentPart1}>
              <BusinessIcon fontSize="large" />
              <Typography variant="h5">Urban Education</Typography>
            </div>
            <div className={classes.listContentPart2}>
              <div className={classes.rightContainer}>
                <School fontSize="large" />
              </div>
              <div className={classes.leftContainer}>
                <LibraryBooks fontSize="large" />
              </div>
            </div>
          </div>
        </ListItem>
      </List>
    </div>
  );
}

export default InstituteList;
