import React from "react";
import clsx from "clsx";
import NotificationIcon from "@material-ui/icons/Notifications";
import MailIcon from "@material-ui/icons/Mail";
import MenuIcon from "@material-ui/icons/Menu";
import { useContext } from "react";
import { Search, AccountCircle } from "@material-ui/icons";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles, fade } from "@material-ui/core/styles";
import { UserContext } from "../context/UserContext";
import { Typography, AppBar, Toolbar, IconButton, InputBase, Badge } from "@material-ui/core";
import { SIDE_DRAWER_WIDTH } from "../styles/styleConstants";

const useStyle = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: SIDE_DRAWER_WIDTH,
    width: `calc(100% - ${SIDE_DRAWER_WIDTH}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    menuButton: {
      marginRight: 36,
    },
  },
  hide: {
    display: "none",
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
}));

function Header(props) {
  const classes = useStyle();
  const { user } = useContext(UserContext);
  const { open, handleDrawerToggle } = props.sideDrawerConfig;

  return (
    <div className={classes.root}>
      <AppBar color="default" position="fixed" className={clsx(classes.appBar, { [classes.appBarShift]: open })}>
        <Toolbar>
          <IconButton aria-label="open Drawer" edge="start" className={clsx(classes.menuButton, { [classes.hide]: open })} onClick={handleDrawerToggle}>
            <MenuIcon />
          </IconButton>
          <Typography color="inherit" variant="h6" noWrap>
            {" "}
            Home{" "}
          </Typography>
          {/* {user.firstName} {user.lastName} */}

          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <Search color="primary" />
            </div>
            <InputBase color="primary" placeholder="search...." classes={{ root: classes.inputRoot, input: classes.inputInput }} inputprops={{ "aria-label": "search" }}></InputBase>
          </div>
          <div className={(classes.grow, classes.desktopIcons)}>
            <div className={classes.sectionDesktop}>
              <IconButton aria-label="show new mails">
                <Badge color="secondary">
                  <MailIcon fontSize="large" />
                </Badge>
              </IconButton>
              <IconButton aria-label="show new notifications">
                <Badge color="secondary">
                  <NotificationIcon fontSize="large"></NotificationIcon>
                </Badge>
              </IconButton>
              <IconButton edge="end">
                <AccountCircle fontSize="large" aria-haspopup="true" />
                <Typography>
                  {" "}
                  {user.firstName} {user.lastName}{" "}
                </Typography>
              </IconButton>
            </div>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default Header;
