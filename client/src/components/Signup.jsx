import React from "react";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { useState } from "react";
import { User } from "../utils/API";

// Bajinder Singh - This component will possibly get depricated and repurposed for other use

const styles = makeStyles((theme) => ({
  main: {
    marginTop: theme.spacing(15),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(2),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  gridContainer: {
    marginBottom: theme.spacing(1),
    marginTop: theme.spacing(1),
  },
  gridItem1: {
    paddingRight: theme.spacing(1),
  },
  gridItem2: {
    paddingLeft: theme.spacing(1),
  },
}));

function Signup() {
  const classes = styles();
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
  });
  function handleInput(event) {
    const input = event.target.id;
    const value = event.target.value;
    setUser((prevUser) => ({
      ...prevUser,
      [input]: value,
    }));
  }

  function handleSubmit(event) {
    console.log("A User: " + user.firstName + "has been Submitted");
    event.preventDefault();
    User.Signup({ user })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <Container className={classes.main} component="main" maxWidth="sm">
      <Typography component="h1" variant="h5">
        Signup
      </Typography>
      <form className={classes.form} onSubmit={handleSubmit}>
        <Grid container className={classes.linkItem} justify="space-between">
          <Grid className={classes.gridItem1} item sm>
            <TextField id="firstName" fullWidth required label="First Name" variant="outlined" autoComplete="First Name" onChange={handleInput} value={user.firstName}></TextField>
          </Grid>
          <Grid className={classes.gridItem2} item sm>
            <TextField id="lastName" fullWidth required label="Last Name" variant="outlined" autoComplete="Last Name" onChange={handleInput} value={user.lastName}></TextField>
          </Grid>
        </Grid>
        <TextField id="email" type="email" fullWidth required margin="normal" label="Email" variant="outlined" autoComplete="Email" onChange={handleInput} value={user.email}></TextField>
        <TextField id="password" fullWidth margin="normal" type="password" required label="Password" variant="outlined" autoComplete="Password" onChange={handleInput} value={user.password}></TextField>
        <TextField id="confirmPassword" fullWidth margin="normal" type="password" required label="Confirm Password" variant="outlined" autoComplete="Confirm Password" onChange={handleInput} value={user.confirmPassword}></TextField>
        <Button type="submit" variant="contained" size="large" fullWidth color="primary" className={classes.submit}>
          Sign Up
        </Button>
      </form>
    </Container>
  );
}
export default Signup;
