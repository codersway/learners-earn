import React from "react";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { withStyles } from "@material-ui/core/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import { darkTheme } from "../styles/darkTheme";

const style = (theme) => ({
  bottomRight: {
    position: "absolute",
    bottom: 10,
    right: 10,
  },
});

class ThemePreference extends React.Component {
  constructor(props) {
    super(props);
    this.state = { darkMode: false };
    this.toggleTheme = this.toggleTheme.bind(this);
  }

  componentDidMount() {
    // When launching application for use. check if there is mode preference set by user
    let savedMode = localStorage.getItem("ThemePreference.darkMode");
    if (savedMode !== null) {
      let darkMode = !(savedMode === "true");
      this.setState({ darkMode }, this.toggleTheme);
    }
  }

  toggleTheme(e) {
    let { darkMode } = this.state; // Toggle Mode
    darkMode = !darkMode;
    if (darkMode) this.props.onThemeChange(createMuiTheme(darkTheme));
    else this.props.onThemeChange(createMuiTheme());
    this.setState({ darkMode });
    localStorage.setItem("ThemePreference.darkMode", darkMode);
  }
  render() {
    const { classes } = this.props;
    return <FormControlLabel className={classes.bottomRight} control={<Switch checked={this.state.darkMode} onChange={this.toggleTheme} />} label="Dark Mode"></FormControlLabel>;
  }
}

export default withStyles(style)(ThemePreference);
