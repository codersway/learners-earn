import React from "react";
import clsx from "clsx";
import { useContext ,useState} from "react";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { Home, Mail, School, PersonAdd, LibraryBooks } from "@material-ui/icons";

import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import { UserContext } from "../context/UserContext";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import { Drawer, IconButton, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import { SIDE_DRAWER_WIDTH } from "../styles/styleConstants";

const useStyle = makeStyles((theme) => ({
  drawer: {
    width: SIDE_DRAWER_WIDTH,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: SIDE_DRAWER_WIDTH,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(7) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
}));

function SideDrawer(props) {
  const theme = useTheme();
  const { open, handleDrawerToggle} = props.config;
  const {handleInstitueClick}=props.instConfig
  const classes = useStyle();
  

  return (
    <Drawer variant="permanent" className={clsx(classes.drawer, { [classes.drawerOpen]: open, [classes.drawerClose]: !open })} classes={{ paper: clsx(classes.drawer, { [classes.drawerOpen]: open, [classes.drawerClose]: !open }) }}>
      <div className={classes.toolbar}>
        <IconButton edge="end" onClick={handleDrawerToggle}>
          {theme.direction === "rtl" ? <ChevronRightIcon /> : <ChevronLeftIcon />}
        </IconButton>
      </div>
      <Divider />
      <List>
        <ListItem button>
          <ListItemIcon>
            <Home />
          </ListItemIcon>
          <ListItemText primary="About us" />
        </ListItem>
        <Divider />
        <ListItem button>
          <ListItemIcon  onClick={handleInstitueClick}>
            <School />
          </ListItemIcon>
          <ListItemText primary="Institute" />
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <LibraryBooks />
          </ListItemIcon>
          <ListItemText primary="Courses" />
        </ListItem>
        <Divider />
        <ListItem button>
          <ListItemIcon>
            <Mail />
          </ListItemIcon>
          <ListItemText primary="Mail" />
        </ListItem>
        <Divider />
        <ListItem button>
          <ListItemIcon>
            <PersonAdd />
          </ListItemIcon>
          <ListItemText primary="Add User" />
        </ListItem>
      </List>
    </Drawer>
  );
}
export default SideDrawer;
