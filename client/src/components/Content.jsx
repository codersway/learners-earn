import React from "react";
import { makeStyle, makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { InstituteCreate, InstituteView, InstituteList } from "../components/Institute";
const useStyle = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
}));
function Content(props) {
  let [addInstitue, setAddInstitue] = React.useState(false);
  const classes = useStyle();
  const handleAddInstitute = () => {
    setAddInstitue(true);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      {props.click && !addInstitue ? <InstituteList config={{ handleAddClick: handleAddInstitute }} /> : null}
      {addInstitue ? <InstituteCreate /> : null}
    </main>
  );
}
export default Content;
