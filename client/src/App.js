import React from "react";
import Login from "./pages/Login";
import CssBaseline from "@material-ui/core/CssBaseline";
import ThemePreference from "./components/ThemePreference";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Signup from "./components/Signup";
import { UserProvider } from "./context/UserContext";
import PrivateRoute from "./utils/PrivateRoute";
import Home from "./pages/Home";

function App(props) {
  const [theme, setTheme] = useState(createMuiTheme());
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline></CssBaseline>
      {/* <AuthContext.Provider value={{ authToken, setAuthToken: setToken,user,setUser}}> */}
      <UserProvider>
        <Router>
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/signup" component={Signup}></Route>
            <PrivateRoute path="/" exact component={Home} />
          </Switch>
        </Router>
        <ThemePreference onThemeChange={setTheme}></ThemePreference>
      </UserProvider>
      {/* </AuthContext.Provider> */}
    </ThemeProvider>
  );
}

export default App;
