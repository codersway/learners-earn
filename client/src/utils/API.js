import axios from "axios";

export const BaseAPI = axios.create({
  baseURL: process.env.REACT_APP_API_URL_BASE,
  responseType: "json",
  headers: {
    api_key: process.env.REACT_APP_API_KEY, // Retrieve the stored session Token
    Authorization: localStorage.getItem("token") ? JSON.parse(localStorage.getItem("token")) : "",
  },
});

export const User = {
  Login: function (data) {
    return BaseAPI.post("/user/login", data);
  },
  Signup: (data) => BaseAPI.post("/user/signup", data),
  Info: (data) => BaseAPI.post("/user/info", data),
  VerifyAuth: (data) => BaseAPI.post("/user/session", data),
  UpdatePassword: (data) => BaseAPI.post("/user/update/password", data),
};
