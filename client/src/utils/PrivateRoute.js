import React from "react";
import { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { UserContext } from "../context/UserContext";

function PrivateRoute(props) {
  const { user } = useContext(UserContext);
  const { component: Component, ...rest } = props;

  return <Route {...rest} render={(props) => (user.loggedIn ? <Component {...props} /> : <Redirect to="/login" />)} />;
}

export default PrivateRoute;
